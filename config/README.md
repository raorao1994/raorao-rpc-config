# raorao Config
[raorao Config]基于Spring Boot Starter的小型Config框架。编写这个Config框架并不是为了重复造轮子，而是出于学习的目的，通过手写一款Config框架来达到知识的学习和应用目的。

## 快速上手
(1) 生成本地Maven依赖
```
# 先下载raorao-config-spring-boot-starter源码
# 然后进入源码pom文件目录，执行命令
mvn clean install
```

(2) 引入Maven依赖
```
<dependency>
    <groupId>com.raorao.config</groupId>
    <artifactId>raorao-config-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```

(3) 同时配置配置中心(请更换为自己的ZK地址)
```
raorao.config.address=192.168.199.241:2181
```


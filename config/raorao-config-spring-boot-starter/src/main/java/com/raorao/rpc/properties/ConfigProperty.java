package com.raorao.rpc.properties;

import com.raorao.rpc.common.constants.ProtocolEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * 参数配置类，实现用户自定义参数
 *
 * @author raorao
 * @since 1.0.0
 */
@EnableConfigurationProperties(ConfigProperty.class)
@ConfigurationProperties("raorao.config")
public class ConfigProperty {

    private static Logger logger = LoggerFactory.getLogger(ConfigProperty.class);

    /**
     * 服务注册中心
     */
    private String address = "127.0.0.1:2181";

    /**
     * 服务协议（暂时只支持zookeeper）：默认走zookeeper，
     */
    private String protocol = ProtocolEnum.ZOOKEPPER.getValue();

    /**
     * 项目编码code
     */
    private String project = "";

    /**
     * 环境
     */
    private String environment = "";

    /**
     * 版本
     */
    private String version = "";

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 判断raorao.config配置信息是否配置
     * @return
     */
    public Boolean validation(){
        if(project.equals("")&&environment.equals("")&&version.equals(""))return false;
        return true;
    }
}

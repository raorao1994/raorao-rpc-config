package com.raorao.rpc.annotation;

import java.lang.annotation.*;

/**
 * 该注解用于注入配置信息
 *
 * @author raorao
 * @since 1.0.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConfigFieldValue {
    String value() default "";
}

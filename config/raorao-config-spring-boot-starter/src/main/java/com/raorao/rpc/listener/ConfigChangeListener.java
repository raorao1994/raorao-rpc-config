package com.raorao.rpc.listener;

import com.raorao.rpc.event.ConfigChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * rpc服务提供者清单变更监听
 * @author raorao
 * @since 1.0.0
 */
@Component
public class ConfigChangeListener implements ApplicationListener<ConfigChangeEvent> {

    private static Logger logger = LoggerFactory.getLogger(ConfigChangeListener.class);

    /**
     * @Async 注解代表该事件为异步处理，要想实现异步，还需要在启动类上增加 @EnableAsync注解
     * @param event
     */
    @Async
    @Override
    public void onApplicationEvent(ConfigChangeEvent event) {
        logger.debug("接收到事件："+event.getClass());
    }
}

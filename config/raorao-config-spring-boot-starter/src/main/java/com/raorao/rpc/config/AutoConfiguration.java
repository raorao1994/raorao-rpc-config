package com.raorao.rpc.config;


import com.raorao.rpc.common.constants.ProtocolEnum;
import com.raorao.rpc.properties.ConfigProperty;
import com.raorao.rpc.server.ConfigFileService;
import com.raorao.rpc.server.DefaultConfigProcessor;
import com.raorao.rpc.server.ZookeeperConfigFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Spring boot 自动配置类
 *
 * @author raorao
 * @since 1.0.0
 */
@Configuration
@EnableAsync
public class AutoConfiguration {

    private static Logger logger = LoggerFactory.getLogger(AutoConfiguration.class);

    @Bean
    public ConfigProperty configProperty() {
        ConfigProperty configProperty = new ConfigProperty();
        if(!configProperty.validation()){
            logger.error("【raorao-config】缺少raorao.config配置信息！！！");
        }
        return configProperty;
    }

    @Bean
    public DefaultConfigProcessor defaultConfigProcessor() {
        return new DefaultConfigProcessor();
    }

    /**
     * 根据参数获取配置中间件服务
     * @param configProperty 参数
     * @return ServiceDiscoverer
     */
    @Bean
    public ConfigFileService configFileService(@Autowired ConfigProperty configProperty){
        ConfigFileService serviceDiscoverer=null;
        switch (ProtocolEnum.fromString(configProperty.getProtocol())){
            case ZOOKEPPER:{
                serviceDiscoverer=new ZookeeperConfigFileService(configProperty);
                break;
            }
            default:{
                serviceDiscoverer=new ZookeeperConfigFileService(configProperty);
                break;
            }
        }
        return serviceDiscoverer;
    }
}

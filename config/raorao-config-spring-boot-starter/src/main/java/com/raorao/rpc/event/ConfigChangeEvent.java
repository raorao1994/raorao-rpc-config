package com.raorao.rpc.event;

import org.springframework.context.ApplicationEvent;

/**
 * 配置变更事件
 * @author raorao
 * @since 1.0.0
 */
public class ConfigChangeEvent extends ApplicationEvent {
    private Integer type;
    private String val;
    public ConfigChangeEvent(Object source, Integer type,String val) {
        super(source);
        this.type=type;
        this.val=val;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}

package com.raorao.rpc.common.utils;

/**
 * @Description : 文件路径帮助类
 * @Author : cxw
 * @Date : 2022/8/12 11:22
 * @Version : 1.0
 **/
public class PathUtil {

    /**
     * 获取工程根目录
     * @return
     */
    public static String getUserDir(){
        return System.getProperty("user.dir");
    }
}

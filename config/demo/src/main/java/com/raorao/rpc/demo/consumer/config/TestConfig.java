package com.raorao.rpc.demo.consumer.config;

import com.raorao.rpc.annotation.RaoRaoConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author raorao
 * @version 1.0
 * @description: 测试类
 * @date 2021/12/9 14:10
 */
@RaoRaoConfig(fileName = "application.yml")
@ConfigurationProperties(prefix = "test")
public class TestConfig {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

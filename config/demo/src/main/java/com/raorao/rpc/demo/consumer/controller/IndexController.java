package com.raorao.rpc.demo.consumer.controller;

import com.raorao.rpc.demo.consumer.config.Test1Config;
import com.raorao.rpc.demo.consumer.config.TestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/index/")
public class IndexController {
    @Autowired
    private Test1Config test1Config;
    @Autowired
    private TestConfig testConfig;
    /**
     * 获取用户信息
     * http://localhost:8080/index/getUser?id=1
     *
     * @param id 用户id
     * @return 用户信息
     */
    @GetMapping("getUser")
    public String getUser(Long id) {
        return "testConfig:"+testConfig.getUrl()+"  test1Config"+test1Config.getUrl();
    }

    @GetMapping("getAdmin")
    public String getAdmin(Long id) {
        return "admin";
    }
}

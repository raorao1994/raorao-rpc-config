package com.raorao.config.config;

import com.raorao.config.common.constants.ProtocolEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author raoroa
 * @version 1.0
 * @description: 配置类
 * @date 2021/11/26 16:55
 */
@Configuration
@ConfigurationProperties(prefix = "raorao.config")
public class ConfigProperty {
    private static Logger logger = LoggerFactory.getLogger(ConfigProperty.class);

    /**
     * 服务注册中心
     */
    private String address = "127.0.0.1:2181";

    /**
     * 服务协议（暂时只支持zookeeper）：默认走zookeeper，
     */
    private String protocol = ProtocolEnum.ZOOKEPPER.getValue();


    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

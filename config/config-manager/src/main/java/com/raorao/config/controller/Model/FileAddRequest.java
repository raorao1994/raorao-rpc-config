package com.raorao.config.controller.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ApiModel("文件对象")
public class FileAddRequest implements Serializable {

    @NotBlank(message = "文件名不能为空！")
    @ApiModelProperty("文件名")
    private String name;
    @Min( value = 0,message = "项目id不能为空！")
    @ApiModelProperty("项目id")
    private Integer projectId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}

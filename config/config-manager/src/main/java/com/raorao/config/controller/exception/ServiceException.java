package com.raorao.config.controller.exception;

/**
 * @author raorao
 * @version 1.0
 * @description: 系统错误信息
 * @date 2021/11/30 16:35
 */
public class ServiceException extends Exception  {
    protected String errorMessage;
    protected String errorCode;
    public ServiceException(String errorMessage,String errorCode){
        this.errorCode=errorCode;
        this.errorMessage=errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}

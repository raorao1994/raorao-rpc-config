package com.raorao.config.dao.pojo;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author raorao
 * @version 1.0
 * @description: 关联关系
 * @date 2021/11/26 16:30
 */
@Entity
@Table(name = "t_related")
public class RelatedInfo extends BasePojo{
    @ApiModelProperty("被引用项目")
    @Column(name = "fromProject",columnDefinition="int(11) COMMENT '被引用项目'")
    private Integer fromProject;

    @ApiModelProperty("被引用文件")
    @Column(name = "fromFile",columnDefinition="int(11) COMMENT '被引用文件'")
    private Integer fromFile;

    @ApiModelProperty("引用项目")
    @Column(name = "toProject",columnDefinition="int(11) COMMENT '引用项目'")
    private Integer toProject;

    @ApiModelProperty("引用文件")
    @Column(name = "toFile",columnDefinition="int(11) COMMENT '引用文件'")
    private Integer toFile;

    public Integer getFromProject() {
        return fromProject;
    }

    public void setFromProject(Integer fromProject) {
        this.fromProject = fromProject;
    }

    public Integer getFromFile() {
        return fromFile;
    }

    public void setFromFile(Integer fromFile) {
        this.fromFile = fromFile;
    }

    public Integer getToProject() {
        return toProject;
    }

    public void setToProject(Integer toProject) {
        this.toProject = toProject;
    }

    public Integer getToFile() {
        return toFile;
    }

    public void setToFile(Integer toFile) {
        this.toFile = toFile;
    }
}

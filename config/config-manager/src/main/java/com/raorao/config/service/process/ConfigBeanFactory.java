package com.raorao.config.service.process;

import com.raorao.config.common.constants.ProtocolEnum;
import com.raorao.config.config.ConfigProperty;
import com.raorao.config.service.register.ServiceRegister;
import com.raorao.config.service.register.ZookeeperServiceRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author raorao
 * @version 1.0
 * @description: 服务注册
 * @date 2021/11/30 14:44
 */
@Component
public class ConfigBeanFactory {
    @Autowired
    private ConfigProperty configProperty;
    /**
     * @description: 获取服务注册类
     * @return: com.raorao.config.service.register.ServiceRegister
     * @author raorao
     * @date: 2021/11/30 14:45
     */
    @Bean
    public ServiceRegister serviceRegister(){
        ServiceRegister register=null;
        String protocol = configProperty.getProtocol();
        ProtocolEnum protocolEnum = ProtocolEnum.fromString(protocol);
        switch (protocolEnum){
            case REDIS:{
                break;
            }
            case ZOOKEPPER:{
                register=new ZookeeperServiceRegister(configProperty);
                break;
            }
            default:{
                register=new ZookeeperServiceRegister(configProperty);
            }
        }
        return register;
    }
}

package com.raorao.config.controller.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@ApiModel("项目信息")
public class ProjectResponse {
    @ApiModelProperty(value="项目id")
    private Integer projectId;
    @ApiModelProperty(value="项目名")
    private String name;
    @ApiModelProperty(value="id")
    private Integer id;
    @ApiModelProperty(value="pid")
    private Integer pid;
    @ApiModelProperty(value="级别")
    private Integer leve;
    @ApiModelProperty(value="文件id")
    private Integer fileId;
    @ApiModelProperty(value="子集")
    private List<ProjectResponse> children=new ArrayList<>();

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getLeve() {
        return leve;
    }

    public void setLeve(Integer leve) {
        this.leve = leve;
    }

    public List<ProjectResponse> getChildren() {
        return children;
    }

    public void setChildren(List<ProjectResponse> children) {
        this.children = children;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }
}

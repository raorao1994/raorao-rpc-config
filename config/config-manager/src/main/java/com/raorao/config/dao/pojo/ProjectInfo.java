package com.raorao.config.dao.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author raorao
 * @version 1.0
 * @description: 项目信息
 * @date 2021/11/26 16:27
 */
@ApiModel("项目信息")
@Entity
@Table(name = "t_project")
public class ProjectInfo extends BasePojo {
    @ApiModelProperty("中文名")
    @Column(name = "cname",columnDefinition="varchar(50) COMMENT '中文名'")
    private String cname;
    @ApiModelProperty("项目编码，唯一值")
    @Column(name = "code",columnDefinition="varchar(50) COMMENT '项目编码，唯一值'")
    private String code;
    @ApiModelProperty("版本号")
    @Column(name = "version",columnDefinition="varchar(50) COMMENT '版本号'")
    private String version;
    @ApiModelProperty("环境")
    @Column(name = "environment",columnDefinition="varchar(50) COMMENT '环境'")
    private String environment;
    @ApiModelProperty("备注")
    @Column(name = "remark",columnDefinition="varchar(50) COMMENT '备注'")
    private String remark;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

package com.raorao.config.common.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 配置变更枚举类
 * @date 2021/11/19 11:19
 */
public enum ConfigChangeEnum {
    /* 基础操作 */
    CONFIG_ADD("新增配置", 1, 1),
    CONFIG_EDIT("修改配置", 2, 2),
    CONFIG_DELETE("删除配置", 3, 3),

    FILE_ADD("新增文件", 11, 4),
    FILE_EDIT("修改文件", 12, 5),
    FILE_DELETE("删除文件", 13, 6),

    PROJECT_ADD("新增项目", 21, 7),
    PROJECT_EDIT("修改项目", 22, 8),
    PROJECT_DELETE("删除项目", 23, 9),
    ;

    private String name;
    private Integer value;
    private Integer sort;

    private ConfigChangeEnum(String name, Integer value, Integer sort) {
        this.name = name;
        this.value = value;
        this.sort = sort;
    }

    public static ConfigChangeEnum fromInt(Integer value) {
        for (ConfigChangeEnum b : ConfigChangeEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}

package com.raorao.config.dao.pojo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * @author raorao
 * @version 1.0
 * @description: 文件对象
 * @date 2021/11/26 15:16
 */
@ApiModel("文件对象")
@Entity
@Table(name = "t_fileInfo")
public class FileInfo extends BasePojo{

    @ApiModelProperty("文件名")
    @Column(name = "name",columnDefinition="varchar(50) COMMENT '文件名'")
    private String name;

    @ApiModelProperty("项目id")
    @Column(name = "projectId",columnDefinition="int(11) COMMENT '项目id'")
    private Integer projectId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Files{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", projectId=" + projectId +
                '}';
    }
}

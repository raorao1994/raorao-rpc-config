package com.raorao.config.dao.repository;

import com.raorao.config.dao.pojo.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 文件
 * @date 2021/11/26 15:28
 */
public interface FileInfoRepository extends JpaRepository<FileInfo,Integer> {
    //方法名称必须要遵循驼峰式命名规则，findBy（关键字）+属性名称（首字母大写）+查询条件（首字母大写）
    List<FileInfo> findByNameAndIsDelete(String name,Boolean isDelete);
    List<FileInfo> findByProjectIdAndIsDelete(Integer projectId,Boolean isDelete);
    FileInfo queryById(Integer id);
    List<FileInfo> findAll();
}

package com.raorao.config.controller.Model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author raorao
 * @version 1.0
 * @description: 业务结果状态（仅返回成功或失败标志是使用（如新增、修改接口、删除接口、验证接口等））
 * @date 2021/11/30 16:24
 */
public class ResultState implements Serializable {
    private static final long serialVersionUID = -4345768455374440091L;

    @ApiModelProperty(value="状态（true false）",example="true")
    private Boolean resultState;

    @ApiModelProperty(value="ID")
    private String id;

    public ResultState() {
        super();
    }

    public ResultState(Boolean resultState) {
        this.resultState = resultState;
    }
    public ResultState(Boolean resultState, String id) {
        this.resultState = resultState;
        this.id = id;
    }

    public static ResultState success() {
        return new ResultState(true);
    }

    public static ResultState success(String id) {
        return new ResultState(true, id);
    }

    public static ResultState fail() {
        return new ResultState(false);
    }

    public Boolean getResultState() {
        return resultState;
    }

    public void setResultState(Boolean resultState) {
        this.resultState = resultState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

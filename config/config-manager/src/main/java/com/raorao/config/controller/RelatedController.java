package com.raorao.config.controller;

import com.raorao.config.controller.Model.ResponseEntity;
import com.raorao.config.controller.Model.ResultState;
import com.raorao.config.dao.pojo.RelatedInfo;
import com.raorao.config.dao.repository.RelatedInfoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 关联关系控制器
 * @date 2021/11/26 14:52
 */
@RequestMapping("related")
@Api(tags = "关联关系控制器")
@RestController
public class RelatedController {
    @Autowired
    private RelatedInfoRepository repository;

    @ApiOperation("添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "关联信息", required = true)
    })
    @PostMapping("add")
    public ResponseEntity<RelatedInfo> add(@Validated @RequestBody RelatedInfo info){
        info.setId(null);
        info.setCreateAt(new Date());
        repository.save(info);
        return ResponseEntity.success(info);
    }

    @ApiOperation("修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "关联信息", required = true)
    })
    @PostMapping("edit")
    public ResponseEntity<RelatedInfo> edit(@Validated @RequestBody RelatedInfo info){
        repository.save(info);
        return ResponseEntity.success(info);
    }

    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true)
    })
    @DeleteMapping("删除")
    public ResponseEntity<ResultState> add(@RequestParam("id") Integer id){
        repository.deleteById(id);
        return ResponseEntity.success(ResultState.success());
    }

    @ApiOperation("根据from项目id和文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ProjectId", value = "项目id", required = true),
            @ApiImplicitParam(name = "fileId", value = "文件id", required = true)
    })
    @GetMapping("queryByFromProjectAndFromFile")
    public ResponseEntity<List<RelatedInfo>> queryByFromProjectAndFromFile(@RequestParam("ProjectId") Integer ProjectId,@RequestParam("fileId")Integer fileId){
        List<RelatedInfo> result = repository.queryByFromProjectAndFromFileAndIsDelete(ProjectId,fileId,false);
        return ResponseEntity.success(result);
    }

    @ApiOperation("根据to项目id和文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ProjectId", value = "项目id", required = true),
            @ApiImplicitParam(name = "fileId", value = "文件id", required = true)
    })
    @GetMapping("queryByToProjectAndToFile")
    public ResponseEntity<List<RelatedInfo>> queryByToProjectAndToFile(@RequestParam("ProjectId") Integer ProjectId,@RequestParam("fileId")Integer fileId){
        List<RelatedInfo> result = repository.queryByToProjectAndToFileAndIsDelete(ProjectId,fileId,false);
        return ResponseEntity.success(result);
    }
}

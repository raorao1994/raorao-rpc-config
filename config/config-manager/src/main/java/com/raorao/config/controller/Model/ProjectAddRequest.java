package com.raorao.config.controller.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ApiModel("项目信息")
public class ProjectAddRequest implements Serializable {

    @NotBlank( message = "中文名不能为空！")
    @ApiModelProperty("中文名")
    private String cname;
    @NotBlank( message = "项目编码不能为空！")
    @ApiModelProperty("项目编码，唯一值")
    private String code;
    @NotBlank( message = "版本号不能为空！")
    @ApiModelProperty("版本号")
    private String version;
    @NotBlank( message = "环境不能为空！")
    @ApiModelProperty("环境")
    private String environment;
    @ApiModelProperty("备注")
    private String remark;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

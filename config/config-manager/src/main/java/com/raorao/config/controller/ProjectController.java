package com.raorao.config.controller;

import com.raorao.config.common.constants.ProjectEnum;
import com.raorao.config.controller.Model.ProjectAddRequest;
import com.raorao.config.controller.Model.ProjectResponse;
import com.raorao.config.controller.Model.ResponseEntity;
import com.raorao.config.controller.Model.ResultState;
import com.raorao.config.dao.pojo.FileInfo;
import com.raorao.config.dao.pojo.ProjectInfo;
import com.raorao.config.dao.repository.FileInfoRepository;
import com.raorao.config.dao.repository.ProjectInfoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author raorao
 * @version 1.0
 * @description: 项目控制器
 * @date 2021/11/26 14:52
 */
@RequestMapping("project")
@Api(tags = "项目控制器")
@RestController
public class ProjectController {
    @Autowired
    private ProjectInfoRepository repository;
    @Autowired
    private FileInfoRepository fileInfoRepository;

    @ApiOperation("添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "项目信息", required = true)
    })
    @PostMapping("add")
    public ResponseEntity<ProjectInfo> add(@Validated ProjectAddRequest info){
        ProjectInfo projectInfo=new ProjectInfo();
        BeanUtils.copyProperties(info,projectInfo);
        projectInfo.setCreateAt(new Date());
        projectInfo.setUpdateAt(new Date());
        projectInfo.setId(null);
        repository.save(projectInfo);
        return ResponseEntity.success(projectInfo);
    }

    @ApiOperation("修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "项目信息", required = true)
    })
    @PostMapping("edit")
    public ResponseEntity<ProjectInfo> edit(@Validated @RequestBody ProjectInfo info){
        repository.save(info);
        return ResponseEntity.success(info);
    }

    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true)
    })
    @DeleteMapping("删除")
    public ResponseEntity<ResultState> add(@RequestParam("id") Integer id){
        repository.deleteById(id);
        return ResponseEntity.success(ResultState.success());
    }

    @ApiOperation("根据项目id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "项目id", required = true)
    })
    @GetMapping("getById")
    public ResponseEntity<ProjectInfo> getById(@RequestParam("id") Integer id){
        ProjectInfo result = repository.getById(id);
        return  ResponseEntity.success(result);
    }

    @ApiOperation("获取所有项目清单")
    @GetMapping("getAll")
    public ResponseEntity<List<ProjectInfo>> getAll(){
        List<ProjectInfo> result = repository.findAll();
        return  ResponseEntity.success(result);
    }

    @ApiOperation("获取所有项目")
    @GetMapping("all")
    public ResponseEntity<List<ProjectResponse>> all(){
        List<ProjectResponse> result=new ArrayList<>();
        List<ProjectInfo> projectInfos = repository.getByIsDelete(false);
        if(projectInfos==null||projectInfos.size()==0)return ResponseEntity.success(result);
        List<FileInfo> fileInfos = fileInfoRepository.findAll();
        Map<Integer,List<FileInfo>> fileMap=new HashMap<>();
        if(fileInfos!=null&&fileInfos.size()>0){
            fileMap=fileInfos.stream().collect(Collectors.groupingBy(FileInfo::getProjectId));
        }
        Map<String, List<ProjectInfo>> collect = projectInfos.stream().collect(Collectors.groupingBy(ProjectInfo::getCname));
        Integer index=1;
        for (String projectName:collect.keySet()){
            List<ProjectInfo> projectInfos1 = collect.get(projectName);
            Integer projectId=projectInfos1.get(0).getId();
            ProjectResponse projectResponse=new ProjectResponse();
            projectResponse.setId(index);index++;
            projectResponse.setName(projectInfos1.get(0).getCname());
            projectResponse.setLeve(ProjectEnum.PROJECT.getValue());
            projectResponse.setProjectId(projectId);
            Map<String, List<ProjectInfo>> collect1 = projectInfos1.stream().collect(Collectors.groupingBy(ProjectInfo::getEnvironment));
            for (String envName:collect1.keySet()){
                List<ProjectInfo> projectInfos2 = collect1.get(envName);
                ProjectResponse envResponse=new ProjectResponse();
                envResponse.setId(index);index++;
                envResponse.setName(projectInfos2.get(0).getEnvironment());
                envResponse.setLeve(ProjectEnum.ENVIRONMENT.getValue());
                envResponse.setPid(projectResponse.getId());
                envResponse.setProjectId(projectId);
                Map<String, List<ProjectInfo>> collect2 = projectInfos2.stream().collect(Collectors.groupingBy(ProjectInfo::getVersion));
                for (String versionName:collect2.keySet()){
                    List<ProjectInfo> projectInfos3 = collect2.get(versionName);
                    for (ProjectInfo item:projectInfos3){
                        ProjectResponse versionResponse=new ProjectResponse();
                        versionResponse.setId(index);index++;
                        versionResponse.setName(item.getVersion());
                        versionResponse.setLeve(ProjectEnum.VERSION.getValue());
                        versionResponse.setProjectId(item.getId());
                        versionResponse.setPid(envResponse.getId());
                        List<FileInfo> fileInfos1 = fileMap.get(item.getId());
                        if(fileInfos1!=null&&fileInfos1.size()>0){
                            for (FileInfo fileInfo:fileInfos1){
                                ProjectResponse fileResponse=new ProjectResponse();
                                fileResponse.setId(index);index++;
                                fileResponse.setName(fileInfo.getName());
                                fileResponse.setLeve(ProjectEnum.FILE.getValue());
                                fileResponse.setProjectId(fileInfo.getId());
                                fileResponse.setPid(versionResponse.getId());
                                fileResponse.setFileId(fileInfo.getId());
                                versionResponse.getChildren().add(fileResponse);
                            }
                        }
                        envResponse.getChildren().add(versionResponse);
                    }
                }
                projectResponse.getChildren().add(envResponse);
            }
            result.add(projectResponse);

        }
        return ResponseEntity.success(result);
    }
}

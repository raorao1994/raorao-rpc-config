package com.raorao.config.common.constants;

/**
 * 通用常量
 *
 * @author raorao
 * @since 1.0.0
 */
public class ConfigConstant {
    private ConfigConstant() {}

    /**
     * Zookeeper服务注册地址
     */
    public static final String ZK_SERVICE_PATH = "/raorao-config";

    /**
     * 编码
     */
    public static final String UTF_8 = "UTF-8";

    /**
     * 路径分隔符
     */
    public static final String PATH_DELIMITER = "/";

    /**
     * 路径分隔符
     */
    public static final String PATH_INFO_PATH = "file";
}

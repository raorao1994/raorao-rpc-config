package com.raorao.config.controller.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;

/**
 * @author raorao
 * @version 1.0
 * @description: 通用接口返回对象
 * @date 2021/11/30 16:23
 */
@ApiModel(value = "通用接口返回对象")
public class ResponseEntity<E> {
    @ApiModelProperty(value="状态码", example="200")
    private Integer code;
    @ApiModelProperty(value="返回信息", example="success")
    private String msg;
    @ApiModelProperty(value="success")
    private Boolean success = Boolean.TRUE;
    @ApiModelProperty(value="返回数据")
    private E data;

    public ResponseEntity() {
        super();
    }

    public ResponseEntity(Integer code, String msg, E data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseEntity(Integer code, String msg, Boolean success, E data) {
        this.code = code;
        this.msg = msg;
        this.success = success;
        this.data = data;
    }



    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public static <E> ResponseEntity<E> success() {
        return new ResponseEntity<>(200, "Success", null);
    }

    public static <E> ResponseEntity<E> success(E data) {
        return new ResponseEntity<>(200, "Success", data);
    }

    public static <E> ResponseEntity<E> success(int code, E data) {
        return new ResponseEntity<>(code, "Success", data);
    }

    public static <E> ResponseEntity<E> success(int code) {
        return new ResponseEntity<>(code, "Success", null);
    }

    public static <E> ResponseEntity<E> success(Integer code, String msg, E data) { return new ResponseEntity<>( null == code ? 200 : code, null != msg && !"".equals(msg.trim()) ? msg : "Success" , data); }

    public static <E> ResponseEntity<E> fail(int code, String msg, E data) {
        return new ResponseEntity<>(code, msg, Boolean.FALSE, data);
    }

    public static <E> ResponseEntity<E> fail(int code, String msg) {
        return new ResponseEntity<>(code, msg, Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> fail(E data) {
        return new ResponseEntity<>(500, "系统开小差啦~~~", Boolean.FALSE, data);
    }

    public static <E> ResponseEntity<E> fail(int code, String msg, HttpServletResponse response) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(code, msg, Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> errorToken() {
        return new ResponseEntity<>(401, "error token", Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> errorCarrier() {
        return new ResponseEntity<>(403, "error carrier", Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> notFund() {
        return new ResponseEntity<>(404, "not fund", Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> error(E data) {
        return new ResponseEntity<>(500, "系统开小差啦~~~~~~~~~~~~~~", Boolean.FALSE, data);
    }

    public static <E> ResponseEntity<E> error(HttpServletResponse response) {
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "系统开小差啦~~~~~~~~~~~~~~", Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> error(String msg, E data) {
        return new ResponseEntity<>(500, msg, Boolean.FALSE, data);
    }

    public static <E> ResponseEntity<E> expireToken() {
        return new ResponseEntity<>(401, " token has expired ", Boolean.FALSE, null);
    }

    public static <E> ResponseEntity<E> notAuth() {
        return new ResponseEntity<>(401, " service not authorized ", Boolean.FALSE, null);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}

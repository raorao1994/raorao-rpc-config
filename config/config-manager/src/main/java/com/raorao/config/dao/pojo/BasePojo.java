package com.raorao.config.dao.pojo;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author raorao
 * @version 1.0
 * @description: TODO
 * @date 2021/11/26 15:24
 */
@MappedSuperclass
public abstract  class BasePojo implements Serializable {

    public BasePojo(){
        this.updateAt=new Date();
    }

    @ApiModelProperty("主键")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",columnDefinition="int(11) COMMENT '主键，自动生成'")
    private Integer id;

    @ApiModelProperty("是否删除")
    @Column(name = "isDelete",columnDefinition="int(4) COMMENT '是否删除'")
    private Boolean isDelete=false;

    @Column(name = "createBy",columnDefinition="varchar(50) COMMENT '创建人'")
    private String createBy;

    @Column(name = "createAt",columnDefinition="date COMMENT '创建时间'")
    private Date createAt;

    @Column(name = "updateBy",columnDefinition="varchar(50) COMMENT '更新人'")
    private String updateBy;

    @Column(name = "updateAt",columnDefinition="date COMMENT '更新时间'")
    private Date updateAt;

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        updateAt=new Date();
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

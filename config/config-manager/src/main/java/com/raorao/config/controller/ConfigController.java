package com.raorao.config.controller;

import com.raorao.config.common.constants.ConfigChangeEnum;
import com.raorao.config.common.utils.EventPushUtil;
import com.raorao.config.controller.Model.ResponseEntity;
import com.raorao.config.controller.Model.ResultState;
import com.raorao.config.dao.pojo.ConfigInfo;
import com.raorao.config.dao.repository.ConfigInfoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 配置信息控制器
 * @date 2021/11/26 14:52
 */
@RequestMapping("config")
@Api(tags = "配置信息控制器")
@RestController
public class ConfigController {
    @Autowired
    private ConfigInfoRepository repository;
    @Autowired
    private EventPushUtil eventPushUtil;

    @ApiOperation("添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "配置信息", required = true)
    })
    @PostMapping("add")
    public ResponseEntity<ConfigInfo> add(@Validated ConfigInfo info){
        info.setId(null);
        info.setCreateAt(new Date());
        repository.save(info);
        eventPushUtil.configChangeEvent(info, ConfigChangeEnum.CONFIG_ADD.getValue());
        return ResponseEntity.success(info);
    }

    @ApiOperation("修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "配置信息", required = true)
    })
    @PostMapping("edit")
    public ResponseEntity<ConfigInfo> edit(@Validated ConfigInfo info){
        ConfigInfo configInfo = repository.queryById(info.getId());
        configInfo.setName(info.getName());
        configInfo.setVal(info.getVal());
        configInfo.setSort(info.getSort());
        configInfo.setType(info.getType());
        configInfo.setUpdateAt(new Date());
        repository.save(configInfo);
        eventPushUtil.configChangeEvent(configInfo, ConfigChangeEnum.CONFIG_EDIT.getValue());
        return ResponseEntity.success(configInfo);
    }

    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true)
    })
    @GetMapping("del")
    public ResponseEntity<ResultState> add(@RequestParam("id") Integer id){
        ConfigInfo info = repository.queryById(id);
        if(info!=null){
            repository.deleteById(id);
            eventPushUtil.configChangeEvent(info, ConfigChangeEnum.CONFIG_DELETE.getValue());
        }
        return ResponseEntity.success(ResultState.success());
    }

    @ApiOperation("根据文件id获取配置清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", required = true)
    })
    @GetMapping("queryByFileId")
    public ResponseEntity<List<ConfigInfo>> queryByFileId(@RequestParam("id") Integer id){
        List<ConfigInfo> result = repository.queryByFileIdAndIsDelete(id,false);
        return ResponseEntity.success(result);
    }

    @ApiOperation("根据id获取配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", required = true)
    })
    @GetMapping("queryById")
    public ResponseEntity<ConfigInfo> queryById(@RequestParam("id") Integer id){
        ConfigInfo result = repository.queryByIdAndIsDelete(id,false);
        return ResponseEntity.success(result);
    }
}

package com.raorao.config.dao.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author raorao
 * @version 1.0
 * @description: 配置类
 * @date 2021/11/26 16:24
 */
@ApiModel("配置信息")
@Entity
@Table(name = "t_config")
public class ConfigInfo extends BasePojo{

    @ApiModelProperty("名称：key")
    @Column(name = "name",columnDefinition="varchar(50) COMMENT '名称'")
    private String name;

    @ApiModelProperty("配置值")
    @Column(name = "val",columnDefinition="varchar(50) COMMENT '配置值'")
    private String val;

    @NotNull(message = "文件id不能为空！")
    @ApiModelProperty("文件id")
    @Column(name = "fileId",columnDefinition="int(11)  COMMENT '文件id'")
    private Integer fileId;

    @ApiModelProperty("排序字段")
    @Column(name = "sort",columnDefinition="int(11)  COMMENT '排序字段'")
    private Integer sort;

    @ApiModelProperty("配置信息类型")
    @Column(name = "type",columnDefinition="varchar(50) COMMENT '配置信息类型'")
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

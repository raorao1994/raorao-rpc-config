package com.raorao.config.dao.repository;

import com.raorao.config.dao.pojo.RelatedInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 文件
 * @date 2021/11/26 15:28
 */
public interface RelatedInfoRepository extends JpaRepository<RelatedInfo,Integer> {
    List<RelatedInfo> queryByFromProjectAndFromFileAndIsDelete(Integer ProjectId,Integer fileId,Boolean isDelete);
    List<RelatedInfo> queryByToProjectAndToFileAndIsDelete(Integer ProjectId,Integer fileId,Boolean isDelete);
}

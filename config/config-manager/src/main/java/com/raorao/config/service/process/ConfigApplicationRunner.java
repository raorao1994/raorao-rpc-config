package com.raorao.config.service.process;

import com.alibaba.fastjson.JSON;
import com.raorao.config.common.constants.ConfigConstant;
import com.raorao.config.dao.pojo.ConfigInfo;
import com.raorao.config.dao.pojo.FileInfo;
import com.raorao.config.dao.pojo.ProjectInfo;
import com.raorao.config.dao.repository.ConfigInfoRepository;
import com.raorao.config.dao.repository.FileInfoRepository;
import com.raorao.config.dao.repository.ProjectInfoRepository;
import com.raorao.config.service.register.ServiceRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 初始化配置类
 * @date 2021/11/30 14:04
 */
@Component
public class ConfigApplicationRunner implements ApplicationRunner {
    @Autowired
    private ProjectInfoRepository projectInfoRepository;
    @Autowired
    private FileInfoRepository fileInfoRepository;
    @Autowired
    private ConfigInfoRepository configInfoRepository;
    @Autowired
    private ServiceRegister serviceRegister;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //清空节点
        serviceRegister.delete(ConfigConstant.ZK_SERVICE_PATH);
        //1、获取所有的项目配置文件
        List<ProjectInfo> projectInfos = projectInfoRepository.findAll();
        if(CollectionUtils.isEmpty(projectInfos))return;
        for(ProjectInfo projectInfo:projectInfos){
            List<FileInfo> fileInfos = fileInfoRepository.findByProjectIdAndIsDelete(projectInfo.getId(),false);
            if(CollectionUtils.isEmpty(fileInfos))continue;
            for (FileInfo fileInfo:fileInfos){
                //2、根据文件清单，生成节点
                this.createConfig(projectInfo,fileInfo);
            }
        }
    }

    /**
     * @description: 根据项目和文件信息，在服务中间件创建配置信息节点
     * @param: projectInfo
     * @param: fileInfo
     * @return: void
     * @author raorao
     * @date: 2021/11/30 14:11
     */
    private void createConfig(ProjectInfo projectInfo, FileInfo fileInfo) {
        //1、根据文件信息，获取所有配置信息
        List<ConfigInfo> configInfos = configInfoRepository.queryByFileIdAndIsDelete(fileInfo.getId(),false);
        if(CollectionUtils.isEmpty(configInfos))return;
        //2、将节点信息注册到分布式中间件中
        for (ConfigInfo configInfo:configInfos){
            StringBuilder path=new StringBuilder();
            path.append(ConfigConstant.ZK_SERVICE_PATH).append(ConfigConstant.PATH_DELIMITER);
            path.append(projectInfo.getCode()).append(ConfigConstant.PATH_DELIMITER);
            path.append(projectInfo.getEnvironment()).append(ConfigConstant.PATH_DELIMITER);
            path.append(projectInfo.getVersion()).append(ConfigConstant.PATH_DELIMITER);
            path.append(ConfigConstant.PATH_INFO_PATH).append(ConfigConstant.PATH_DELIMITER);
            path.append(fileInfo.getName()).append(ConfigConstant.PATH_DELIMITER);
            path.append(configInfo.getName());
            //创建配置模型
            ConfigModel configModel=new ConfigModel();
            configModel.setName(configInfo.getName());
            configModel.setVal(configInfo.getVal());
            configModel.setType(configInfo.getType());
            String val= JSON.toJSONString(configModel);
            serviceRegister.save(path.toString(),val);
        }
    }
}

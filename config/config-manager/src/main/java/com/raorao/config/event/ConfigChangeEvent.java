package com.raorao.config.event;

import org.springframework.context.ApplicationEvent;

/**
 * 配置变更事件
 * @author raorao
 * @since 1.0.0
 */
public class ConfigChangeEvent extends ApplicationEvent {
    private Integer type;
    public ConfigChangeEvent(Object source, Integer type) {
        super(source);
        this.type=type;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}

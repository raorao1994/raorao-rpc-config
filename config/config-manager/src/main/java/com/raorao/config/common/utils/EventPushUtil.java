package com.raorao.config.common.utils;

import com.raorao.config.event.ConfigChangeEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * 事件发布工具类
 *
 * @author raorao
 * @since 1.0.0
*/
@Component
public class EventPushUtil implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    /**
     * 配置改变事件
     *
     * @param data 数据
     * @param type 事件类型
     */
    public void configChangeEvent(Object data, Integer type){
        // 发布事件
        publisher.publishEvent(new ConfigChangeEvent(data,type));
    }
}

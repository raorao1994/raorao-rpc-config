package com.raorao.config.common.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 项目数据类型枚举
 * @date 2021/11/19 11:19
 */
public enum ProjectEnum {
    /* 基础操作 */
    PROJECT("project", 1, 1),
    ENVIRONMENT("environment", 2, 2),
    VERSION("version", 3, 3),
    FILE("file", 4, 4),
    ;

    private String name;
    private Integer value;
    private Integer sort;

    private ProjectEnum(String name, Integer value, Integer sort) {
        this.name = name;
        this.value = value;
        this.sort = sort;
    }

    public static ProjectEnum fromString(Integer text) {
        for (ProjectEnum b : ProjectEnum.values()) {
            if (b.value.equals(text)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}

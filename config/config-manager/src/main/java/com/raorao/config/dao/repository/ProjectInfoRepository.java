package com.raorao.config.dao.repository;

import com.raorao.config.dao.pojo.ProjectInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author raorao
 * @version 1.0
 * @description: 文件
 * @date 2021/11/26 15:28
 */
public interface ProjectInfoRepository extends JpaRepository<ProjectInfo,Integer> {
    ProjectInfo getById(Integer id);

    List<ProjectInfo> getByIsDelete(Boolean isDelete);
}

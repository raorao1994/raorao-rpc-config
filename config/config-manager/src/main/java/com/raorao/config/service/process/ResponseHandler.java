package com.raorao.config.service.process;

import com.alibaba.fastjson.JSON;
import com.raorao.config.controller.Model.ResponseEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 响应处理器
 * @version 1.0
 * @description: TODO
 * @date 2021/11/30 16:26
 */
public class ResponseHandler {
    private static final Log LOGGER = LogFactory.getLog(ResponseHandler.class);
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final String DEFAULT_CONTENT_TYPE = "application/json;chartset=UTF-8";

    private ResponseHandler() {
    }

    public static void response(HttpServletResponse response, HttpStatus status, ResponseEntity<?> entity) throws IOException {
        response.setStatus(status.value());
        response.setContentType(DEFAULT_CONTENT_TYPE);
        response.setCharacterEncoding(DEFAULT_ENCODING);
        response.getWriter().print(JSON.toJSONString(entity));
        LOGGER.info("ResponseHandler.response Access Token is Empty.");
        response.getWriter().flush();
        response.getWriter().close();
    }

    public static void response(HttpServletResponse response, ResponseEntity<?> entity) throws IOException {
        response.setContentType(DEFAULT_CONTENT_TYPE);
        response.setCharacterEncoding(DEFAULT_ENCODING);
        response.getWriter().print(JSON.toJSONString(entity));
        LOGGER.info("BeforeRequestInterceptor.response entity.");
        response.getWriter().flush();
        response.getWriter().close();
    }
}

package com.raorao.config.service.register;

/**
 * @author raorao
 * @version 1.0
 * @description: 配置注册服务
 * @date 2021/11/26 17:03
 */
public interface ServiceRegister {
    Boolean save(String path,String data);
    Boolean delete(String path);
}

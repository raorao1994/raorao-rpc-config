package com.raorao.cache.common.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 协议类型枚举
 * @date 2021/11/19 11:19
 */
public enum ProtocolEnum {
    /* 基础操作 */
    ZOOKEPPER("zookeeper", "zookeeper", 1),
    REDIS("redis", "redis", 2),
    ;

    private String name;
    private String value;
    private Integer sort;

    private ProtocolEnum(String name, String value, Integer sort) {
        this.name = name;
        this.value = value;
        this.sort = sort;
    }

    public static ProtocolEnum fromString(String text) {
        for (ProtocolEnum b : ProtocolEnum.values()) {
            if (b.value.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}

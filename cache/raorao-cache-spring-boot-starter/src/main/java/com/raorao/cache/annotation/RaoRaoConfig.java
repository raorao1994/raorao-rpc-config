package com.raorao.cache.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 被该注解标记的类加载那个配置文件
 *
 * @author raorao
 * @since 1.0.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface RaoRaoConfig {
    String fileName() default "";
}

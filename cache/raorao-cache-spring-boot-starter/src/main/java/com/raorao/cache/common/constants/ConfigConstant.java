package com.raorao.cache.common.constants;

/**
 * 通用常量
 *
 * @author raorao
 * @since 1.0.0
 */
public class ConfigConstant {
    private ConfigConstant() {}

    /**
     * Zookeeper服务注册地址
     */
    public static final String ZK_SERVICE_PATH = "/raorao-config";

    /**
     * 编码
     */
    public static final String UTF_8 = "UTF-8";

    /**
     * 路径分隔符
     */
    public static final String PATH_DELIMITER = "/";

    /**
     * 路径分隔符
     */
    public static final String PATH_INFO_PATH = "file";

    /**
     * 配置信息分隔符
     */
    public static final String CONFIG_DELIMITER = ".";

    /**
     * 默认文件
     */
    public static final String DEFAULT_FILE = "application.properties";

    /**
     * 默认文件路径分隔符
     */
    public static final String DEFAULT_FILE_PATH_DELIMITER = System.getProperty("file.separator");

    /**
     * 默认文件路径分隔符
     */
    public static final String DEFAULT_FILE_SUFFIX = ".properties";
}

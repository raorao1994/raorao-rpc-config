package com.raorao.cache.server;

import com.raorao.cache.server.model.ConfigModel;

import java.util.Map;

/**
 * 服务发现抽象类，定义服务发现规范
 *
 * @author raorao
 * @since 1.0.0
 */
public interface ConfigFileService {

    /**
     * @description:获取配置文件
     * @param: fileName
     * @return: java.util.Map<java.lang.String,com.raorao.rpc.client.ConfigModel>
     * @author raorao
     * @date: 2021/12/9 15:17
     */
    Map<String, ConfigModel> getConfigFile(String fileName);

    /**
     * @description:保存配置文件
     * @param: fileName
     * @param: data
     * @author raorao
     * @date: 2021/12/9 15:17
     */
    void saveConfigFile(String fileName,String data);

    /**
     * @description:监听配置节点
     * @param: fileName
     * @author raorao
     * @date: 2021/12/9 15:17
     */
    void watchConfig(String fileName,Object bean);
}

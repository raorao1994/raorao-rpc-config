package com.raorao.cache.exception;

/**
 * 异常
 *
 * @author raorao
 * @since 1.0.0
 */
public class RpcException extends RuntimeException {
    public RpcException(String message) {
        super(message);
    }
}

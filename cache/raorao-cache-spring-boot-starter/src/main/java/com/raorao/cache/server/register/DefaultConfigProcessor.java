package com.raorao.cache.server.register;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.*;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * config处理者，自动下载配置信息、监听配置信息
 *
 * @author raorao
 * @since 1.0.0
 */
public class DefaultConfigProcessor implements ApplicationContextInitializer, ApplicationListener<ApplicationEvent>{

    private static Logger logger = LoggerFactory.getLogger(DefaultConfigProcessor.class);

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        logger.debug("【DefaultConfigProcessor-initialize】");
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

        if (applicationEvent instanceof ContextRefreshedEvent) {
            logger.debug("初始化配置中心客户端监听器...ContextRefreshedEvent");
            // 开启服务、下载配置文件
            downLoadConfig(applicationEvent);
            // 监听配置变更
            watchConfig(applicationEvent);
        }else if (applicationEvent instanceof ContextClosedEvent) {
            logger.debug("初始化配置中心客户端监听器...ContextClosedEvent");
        }else {
            logger.debug("初始化配置中心客户端监听器...applicationEvent instanceof RefreshEvent");
        }
    }

    /**
     * @description: 监听配置节点
     * @param: context
     * @return: void
     * @author raorao
     * @date: 2021/12/2 10:24
     */
    private void watchConfig(ApplicationEvent context) {

    }

    /**
     * @description: 下载配置文件
     * @param: context
     * @return: void
     * @author raorao
     * @date: 2021/12/2 10:24
     */
    private void downLoadConfig(ApplicationEvent context) {

    }

//    private void startServer(ApplicationContext context) {
//        Map<String, Object> beans = context.getBeansWithAnnotation(RaoRaoConfig.class);
//        if (beans.size() != 0) {
//            boolean startServerFlag = true;
//            for (Object obj : beans.values()) {
//                try {
//                    Class<?> clazz = obj.getClass();
//                    Class<?>[] interfaces = clazz.getInterfaces();
//                    ServiceObject so;
//                    if (interfaces.length != 1) {
//                        RaoRaoConfig service = clazz.getAnnotation(RaoRaoConfig.class);
//                        String value = service.fileName();
//                        if (value.equals("")) {
//                            startServerFlag = false;
//                            throw new UnsupportedOperationException("The exposed interface is not specific with '" + obj.getClass().getName() + "'");
//                        }
//                        so = new ServiceObject(value, Class.forName(value), obj);
//                    } else {
//                        Class<?> superClass = interfaces[0];
//                        so = new ServiceObject(superClass.getName(), superClass, obj);
//                    }
//                    serviceRegister.register(so);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            if (startServerFlag) {
////                rpcServer.start();
//            }
//        }
//    }
//
//    private void injectService(ApplicationContext context) {
//        String[] names = context.getBeanDefinitionNames();
//        for (String name : names) {
//            Class<?> clazz = context.getType(name);
//            if (Objects.isNull(clazz)) continue;
//            Field[] fields = clazz.getDeclaredFields();
//            for (Field field : fields) {
//                ConfigFieldValue injectLeisure = field.getAnnotation(ConfigFieldValue.class);
//                if (Objects.isNull(injectLeisure)) continue;
//                Class<?> fieldClass = field.getType();
//                Object object = context.getBean(name);
//                field.setAccessible(true);
//                try {
//                    field.set(object, clientProxyFactory.getProxy(fieldClass));
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}

package com.raorao.cache.common.utils;

import com.raorao.cache.event.ConfigChangeEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * 事件发布工具类
 *
 * @author raorao
 * @since 1.0.0
*/
@Component
public class EventPushUtil implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    /**
     * rpc服务提供者改变事件
     *
     * @param path 路径
     * @param type 类型
     * @param val 值
     */
    public void configChangeEvent(String path,Integer type,String val){
        // 发布事件
        publisher.publishEvent(new ConfigChangeEvent(path,type,val));
    }
}

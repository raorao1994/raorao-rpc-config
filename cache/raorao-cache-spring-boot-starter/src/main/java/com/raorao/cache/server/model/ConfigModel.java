package com.raorao.cache.server.model;

import java.io.Serializable;

/**
 * @author raorao
 * @version 1.0
 * @description: 配置信息存储模型
 * @date 2021/12/9 15:06
 */
public class ConfigModel implements Serializable {
    private String name;
    private String val;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

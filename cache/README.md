# raorao cache
[raorao cache] 轻量化分布式对象缓存插件

## 快速上手
(1) 生成本地Maven依赖
```
# 先下载raorao-cache-spring-boot-starter源码
# 然后进入源码pom文件目录，执行命令
mvn clean install
```

(2) 引入Maven依赖
```
<dependency>
    <groupId>com.raorao.cache</groupId>
    <artifactId>raorao-cache-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```


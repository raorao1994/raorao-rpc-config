package com.raorao.rpc.demo.provider.service;

import com.raorao.rpc.annotation.RpcService;
import com.raorao.rpc.demo.api.AdminService;
import com.raorao.rpc.demo.api.ApiResult;
import com.raorao.rpc.demo.api.User;

@RpcService
public class AdminServiceImpl implements AdminService {
    @Override
    public ApiResult<User> getAdmin(Long id) {
        User user = getFromDbOrCache(id);
        return ApiResult.success(user);
    }

    private User getFromDbOrCache(Long id) {
        return new User(id, "baidu", 1, "https://baidu.com");
    }
}

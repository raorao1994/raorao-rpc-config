package com.raorao.rpc.demo.provider.service;

import com.raorao.rpc.demo.api.ApiResult;
import com.raorao.rpc.demo.api.User;
import com.raorao.rpc.demo.api.UserService;
import com.raorao.rpc.annotation.RpcService;

@RpcService
public class UserServiceImpl implements UserService {
    @Override
    public ApiResult<User> getUser(Long id) {
        User user = getFromDbOrCache(id);
        return ApiResult.success(user);
    }

    private User getFromDbOrCache(Long id) {
        return new User(id, "baidu", 1, "https://baidu.com");
    }
}

package com.raorao.rpc.event;

import org.springframework.context.ApplicationEvent;

/**
 * rpc服务提供者清单变更事件
 * @author raorao
 * @since 1.0.0
 */
public class ServiceChangeEvent extends ApplicationEvent {
    private Integer type;
    public ServiceChangeEvent(Object source,Integer type) {
        super(source);
        this.type=type;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}

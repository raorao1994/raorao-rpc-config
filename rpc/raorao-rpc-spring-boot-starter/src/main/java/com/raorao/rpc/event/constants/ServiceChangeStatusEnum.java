package com.raorao.rpc.event.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 服务状态变更枚举
 * @date 2021/11/19 11:19
 */
public enum  ServiceChangeStatusEnum {
    /* 基础操作 */
    ADD("新增", -1, 1),
    EDIT("修改", 0, 2),
    DELETE("删除", 1, 3),
    ;

    private String name;
    private Integer value;
    private Integer sort;

    private ServiceChangeStatusEnum(String name, Integer value, Integer sort) {
        this.name = name;
        this.value = value;
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}

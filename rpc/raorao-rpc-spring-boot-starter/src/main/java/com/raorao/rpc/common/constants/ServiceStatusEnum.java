package com.raorao.rpc.common.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 服务状态枚举
 * @date 2021/11/19 11:19
 */
public enum ServiceStatusEnum {
    /* 基础操作 */
    ERRO("错误", -2, 0),
    NETWORKERRO("网络连接不正常", -1, 1),
    STOP("停用", 0, 0),
    NORMAL("正常", 1, 10),
    GOOD("健康", 2, 30),
    ;

    private String name;
    private Integer value;
    private Integer weight;

    private ServiceStatusEnum(String name, Integer value, Integer weight) {
        this.name = name;
        this.value = value;
        this.weight = weight;
    }

    public static ServiceStatusEnum fromVale(Integer value) {
        for (ServiceStatusEnum b : ServiceStatusEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}

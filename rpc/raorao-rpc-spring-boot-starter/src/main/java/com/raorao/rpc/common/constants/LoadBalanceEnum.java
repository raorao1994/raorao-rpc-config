package com.raorao.rpc.common.constants;

/**
 * @author raorao
 * @version 1.0.0
 * @description: 负载均很枚举
 * @date 2021/11/19 11:19
 */
public enum LoadBalanceEnum {
    /* 基础操作 */
    ROUNDROBIN("轮询（Round Robin）法", "roundrobin", 1),
    RANDOM("随机（Random）法", "random", 2),
    WEIGHRROUNDROBIN("加权轮询（Weight Round Robin）法", "weightroundrobin", 2),
    WEIGHRROUND("加权随机（Weight Random）法", "weightround", 2),
    ;

    private String name;
    private String value;
    private Integer sort;

    private LoadBalanceEnum(String name, String value, Integer sort) {
        this.name = name;
        this.value = value;
        this.sort = sort;
    }

    public static LoadBalanceEnum fromString(String text) {
        for (LoadBalanceEnum b : LoadBalanceEnum.values()) {
            if (b.value.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}

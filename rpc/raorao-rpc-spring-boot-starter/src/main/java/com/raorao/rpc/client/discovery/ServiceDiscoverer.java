package com.raorao.rpc.client.discovery;


import com.raorao.rpc.common.service.ServiceInfo;

import java.util.List;

/**
 * 服务发现抽象类，定义服务发现规范
 *
 * @author raorao
 * @since 1.0.0
 */
public interface ServiceDiscoverer {

    List<ServiceInfo> getServices(String name);

    void initServices();
}

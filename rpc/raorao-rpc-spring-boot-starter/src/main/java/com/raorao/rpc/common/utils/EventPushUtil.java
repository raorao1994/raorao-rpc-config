package com.raorao.rpc.common.utils;

import com.raorao.rpc.common.service.ServiceInfo;
import com.raorao.rpc.event.ServiceChangeEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 * 事件发布工具类
 *
 * @author raorao
 * @since 1.0.0
*/
public class EventPushUtil implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    /**
     * rpc服务提供者改变事件
     *
     * @param serviceInfo 上下文
     * @param type ByteBuf
     */
    public static void serviceChangeEvent(ServiceInfo serviceInfo,Integer type){
        // 发布事件
        publisher.publishEvent(new ServiceChangeEvent(serviceInfo,type));
    }
}

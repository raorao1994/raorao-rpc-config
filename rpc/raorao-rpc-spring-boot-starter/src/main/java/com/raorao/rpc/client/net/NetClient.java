package com.raorao.rpc.client.net;

import com.raorao.rpc.common.service.ServiceInfo;

/**
 * 网络请求客户端，定义网络请求规范
 *
 * @author raorao
 * @since 1.0.0
 */
public interface NetClient {
    byte[] sendRequest(byte[] data, ServiceInfo service) throws InterruptedException;
}

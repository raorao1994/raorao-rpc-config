package com.raorao.rpc.demo.consumer.controller;

import com.raorao.rpc.demo.api.AdminService;
import com.raorao.rpc.demo.api.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.raorao.rpc.demo.api.User;
import com.raorao.rpc.demo.api.UserService;
import com.raorao.rpc.annotation.InjectService;

@RestController
@RequestMapping("/index/")
public class IndexController {

    @InjectService
    private UserService userService;
    @InjectService
    private AdminService adminService;

    /**
     * 获取用户信息
     * http://localhost:8080/index/getUser?id=1
     *
     * @param id 用户id
     * @return 用户信息
     */
    @GetMapping("getUser")
    public ApiResult<User> getUser(Long id) {
        return userService.getUser(id);
    }

    @GetMapping("getAdmin")
    public ApiResult<User> getAdmin(Long id) {
        return adminService.getAdmin(id);
    }
}

package com.raorao.rpc.demo.consumer.service;

import com.raorao.rpc.annotation.RpcService;
import com.raorao.rpc.demo.api.ApiResult;
import com.raorao.rpc.demo.api.User;
import com.raorao.rpc.demo.api.UserService;

@RpcService
public class UserServiceImpl implements UserService {
    @Override
    public ApiResult<User> getUser(Long id) {
        User user = getFromDbOrCache(id);
        return ApiResult.success(user);
    }

    private User getFromDbOrCache(Long id) {
        return new User(id, "1232131", 1, "https://123123213.com");
    }
}

package com.raorao.rpc.demo.api;

public interface AdminService {
    ApiResult<User> getAdmin(Long id);
}

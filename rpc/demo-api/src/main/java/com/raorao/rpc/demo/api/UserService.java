package com.raorao.rpc.demo.api;

public interface UserService {
    ApiResult<User> getUser(Long id);
}
